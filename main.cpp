/**
 * @file main.cpp
 * @brief Main entry of the program.
 */
#include <cstdint>
#include <iostream>
#include <format>
#include <memory>
#include <print>

#if 0
struct Example
{
    Example() noexcept : m_num{} { std::puts("Constructor called"); }
    explicit Example(const std::uint32_t num) noexcept : m_num{num} { std::puts("Constructor with parameters called"); }
    Example(const Example& other) noexcept : m_num{other.m_num} { std::puts("Copy constructor called"); }
    Example(const Example&& other) noexcept : m_num{other.m_num} { std::puts("Move constructor called"); }
    auto& operator=(const Example& other) noexcept { std::puts("Copy assignment operator called"); return *this; }
    auto& operator=(Example&& other) noexcept { std::puts("Move assignment operator called"); return *this; }
    ~Example() { std::puts("Destructor called"); }

    [[nodiscard]]
    constexpr auto getValue() const noexcept { return m_num; }

private:
    std::uint32_t m_num;
};
#endif


/**
* @brief Main function for the program
* @details This function is the entry point for the program.
* @return Returns 0 if the program runs successfully.
*/
auto main() -> std::int32_t
{
    constexpr std::uint32_t cl_num = 1;

    std::println("Printing value of variable: {}", cl_num);
}
