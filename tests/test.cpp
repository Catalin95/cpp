/**
 * @file test.cpp
 * @brief This file contains the tests for the program.
 */

#include "mock.hpp"
#include "work.hpp"

#include <gtest/gtest.h>

#include <gmock/gmock.h>
#include <format>
#include <sstream>
#include <string_view>

using namespace testing;


TEST(ExampleTest, TestLambdaCall)
{
    // redirect std::cout to a stringstream
    const std::stringstream l_buffer{};
    auto* prevCoutBuf{std::cout.rdbuf()};
    std::cout.rdbuf(l_buffer.rdbuf());


    // invoke the lambda
    constexpr std::uint32_t l_num{10};
    const auto l_lambda{[l_num]{ std::cout << std::format("{}", l_num); }};
    l_lambda();

    // restore std::cout to its original state
    std::cout.rdbuf(prevCoutBuf);

    // compare the output written to sstream
    const auto l_expectedOutput{std::to_string(l_num)};

    EXPECT_EQ(l_buffer.str(), l_expectedOutput);
}

auto func(std::uint32_t input)
{
    std::cout << std::format("{}\n", input);
}


TEST(ExampleTest2, ReferenceInput)
{
    static_assert(std::is_same_v<decltype(func), void(std::uint32_t)>);
    ASSERT_TRUE((std::is_same_v<decltype(func), void(std::uint32_t)>));
}