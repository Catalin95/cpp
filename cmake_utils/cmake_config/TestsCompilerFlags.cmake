# Cmake file containing compiler flags for uts

set(CLANG_TESTS_DEBUG_FLAGS "-O0 \
-g \
-ggdb \
-Wall \
-Wextra \
-Wpedantic \
-Wconversion \
-Wextra-semi-stmt \
-Wfloat-equal \
-Wformat=2 \
-Wlogical-op-parentheses \
-Wshadow \
-Wabstract-vbase-init \
-Wcast-qual \
-Warray-bounds-pointer-arithmetic \
-Wcomma -Wconditional-uninitialized \
-Wdelete-non-virtual-dtor \
-Wfour-char-constants \
-Wglobal-constructors \
-Wmissing-field-initializers \
-Wnon-virtual-dtor \
-Wold-style-cast \
-Woverloaded-virtual \
-Wthread-safety-attributes \
-Wunreachable-code \
-Wzero-as-null-pointer-constant \
-Wzero-length-array \
-fprofile-instr-generate \
-fcoverage-mapping \
-fsanitize=address \
-fsanitize=leak \
-fsanitize=undefined")

set(GCC_TESTS_DEBUG_FLAGS "-O0 \
-g \
-ggdb \
-Wall \
-Wextra \
-Wpedantic \
-Wconversion \
-Wfloat-equal \
-Wformat=2 \
-Wparentheses \
-Wshadow \
-Wcast-qual \
-Warray-bounds \
-Wdeprecated \
-Wdeprecated-declarations \
-Wdelete-non-virtual-dtor \
-Wmissing-field-initializers \
-Wnon-virtual-dtor \
-Wold-style-cast \
-Woverloaded-virtual \
-Wunreachable-code \
-Wzero-as-null-pointer-constant \
-Wzero-length-bounds -fprofile-arcs  \
-ftest-coverage \
-fsanitize=address \
-fsanitize=leak \
-fsanitize=undefined")

set(MSVC_TESTS_DEBUG_FLAGS "/Od
/Zi \
/Wall \
/WX \
/sdl \
/RTC1 \
/MP \
/FC \
/analyze:stacksize \
/INCREMENTAL:NO \
/COVERAGE")
