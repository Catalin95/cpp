# Cmake containing coverage functions

function(set_coverage_with_clang program)
    add_custom_target(coverage
        COMMAND ${CMAKE_COMMAND} -E env LLVM_PROFILE_FILE=default.profraw $<TARGET_FILE:${program}>
        COMMAND llvm-profdata merge -sparse default.profraw -o default.profdata
        COMMAND llvm-cov show -format=html -instr-profile=default.profdata -o coverage_report $<TARGET_FILE:${Program}> --ignore-filename-regex '/usr/*' --use-color
        WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
        COMMENT "Generating code coverage report using LLVM tools"
    )
endfunction()

function(set_coverage_with_gcc program)
     add_custom_target(coverage
         COMMAND ${CMAKE_COMMAND} --build ${CMAKE_BINARY_DIR} --target ${program}
         COMMAND ${CMAKE_COMMAND} -E env ${CMAKE_BINARY_DIR}/${Program}/tests
         COMMAND lcov --capture --directory . --output-file coverage.info --rc geninfo_unexecuted_blocks=1
         COMMAND lcov --remove coverage.info '${CMAKE_BINARY_DIR}/*' '/usr/*' --output-file coverage.info --rc lcov_excl_line='assert' --rc lcov_excl_start 'assert' --rc lcov_excl_stop 'end'
         COMMAND genhtml coverage.info --output-directory coverage_report
         WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
         COMMENT "Generating code coverage report using GCC tools"
     )
endfunction()

function(set_coverage_with_msvc program)
    find_program(LLVM_PROFDATA llvm-profdata HINTS ENV LLVM_DIR PATH_SUFFIXES bin)
    find_program(LLVM_COV llvm-cov HINTS ENV LLVM_DIR PATH_SUFFIXES bin)

    if(NOT LLVM_PROFDATA OR NOT LLVM_COV)
        message(FATAL_ERROR "LLVM tools not found. Please ensure llvm-profdata and llvm-cov are installed and in your PATH.")
        return()
    endif()

    # Create a custom target for running tests and generating .profraw file
    add_custom_target(run_tests
        COMMAND ${CMAKE_COMMAND} --build ${CMAKE_BINARY_DIR} --target ${program}
        COMMAND ${CMAKE_COMMAND} -E env LLVM_PROFILE_FILE=${CMAKE_BINARY_DIR}/default.profraw $<TARGET_FILE:${program}>
        WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
        COMMENT "Running tests"
    )
        # Create a custom target for coverage (MSVC)
    add_custom_target(coverage
        DEPENDS run_tests
        COMMAND ${CMAKE_COMMAND} -E echo "Generating coverage report using LLVM tools for MSVC"
        COMMAND ${CMAKE_COMMAND} -E sleep 1  # Give some time for file system to catch up
        COMMAND ${CMAKE_COMMAND} -E touch ${CMAKE_BINARY_DIR}/default.profraw  # Ensure the file exists
        COMMAND ${CMAKE_COMMAND} -E echo "Profiling data file created: ${CMAKE_BINARY_DIR}/default.profraw"
        COMMAND "${LLVM_PROFDATA}" merge -sparse ${CMAKE_BINARY_DIR}/default.profraw -o ${CMAKE_BINARY_DIR}/default.profdata
        COMMAND ${CMAKE_COMMAND} -E echo "Profiling data merged: ${CMAKE_BINARY_DIR}/default.profdata"
        COMMAND "${LLVM_COV}" show -format=html -instr-profile=${CMAKE_BINARY_DIR}/default.profdata -o ${CMAKE_BINARY_DIR}/coverage_report $<TARGET_FILE:${Program}> --ignore-filename-regex 'C:/Program Files/*' --use-color
        WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
        COMMENT "Generating code coverage report using LLVM tools for MSVC"
    )
endfunction()

function(enable_coverage program)
    if (NOT CMAKE_CXX_COMPILER_ID)
        message(WARNING "CMAKE_CXX_COMPILER_ID not set")
        return()
    endif ()

    if (NOT CMAKE_BUILD_TYPE MATCHES "Debug")
        message(WARNING "Code coverage is only supported in Debug mode")
        return()
    endif ()

    if(CMAKE_CXX_COMPILER_ID MATCHES "Clang")
        set_coverage_with_clang(${program})
    elseif(CMAKE_CXX_COMPILER_ID MATCHES "GNU")
        set_coverage_with_gcc(${program})
    elseif(CMAKE_CXX_COMPILER_ID MATCHES "MSVC")
        set_coverage_with_msvc(${program})
    else()
        message(WARNING "Code coverage is only supported for Clang, GCC or MSVC compilers.")
        return()
    endif()
    # Add a dependency so 'make coverage' also runs tests
    add_dependencies(coverage ${program})
endfunction()
