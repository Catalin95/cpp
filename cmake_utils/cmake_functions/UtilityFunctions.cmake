# Cmake helper functions

function(include_all_cmake_files dir)
    # Get a list of all .cmake files in the specified directory
    file(GLOB cmake_files "${dir}/*.cmake")

    # Iterate over each file and include it
    foreach(cmake_file ${cmake_files})
        message(STATUS "Including ${cmake_file}")
        include(${cmake_file})
    endforeach()
endfunction()
