# Cmake fle containing functions for platform

function(set_platform platform)
    if (WIN32)
        set(${platform} "Windows" PARENT_SCOPE)
        return()
    elseif (UNIX)
        set(${platform} "Linux" PARENT_SCOPE)
        return()
    endif ()

    set(${platform} "Unknown" PARENT_SCOPE)
endfunction()
