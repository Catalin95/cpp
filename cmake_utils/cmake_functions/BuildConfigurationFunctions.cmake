# Cmake containing functions for build type

function(set_build_type build_type)
    if (NOT CMAKE_BUILD_TYPE)
        message(STATUS "CMAKE_BUILD_TYPE not set, defaulting to Debug")
        set(${build_type} "Debug" PARENT_SCOPE)
    endif ()
endfunction()
