# Cmake containing dynamic analyzers functions

function(use_valgrind is_valgrind_found)
    find_program(VALGRIND valgrind)

    if (NOT VALGRIND)
        message(WARNING "Valgrind not found. Memory leak checks will not be performed.")
        set(${is_valgrind_found} FALSE PARENT_SCOPE)
        return()
    endif ()

    message(STATUS "Valgrind found: ${VALGRIND}")
    set(${is_valgrind_found} TRUE PARENT_SCOPE)
endfunction()
