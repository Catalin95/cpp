# Cmake containing utility analizers for main app

function(enable_iwyu_for_main program)
    find_program(IWYU_PATH NAMES iwyu iwyu_tool)

    if (NOT IWYU_PATH)
        message(WARNING "IWYU tool not found")
        return()
    endif ()

    set(CMAKE_CXX_INCLUDE_WHAT_YOU_USE "${IWYU_PATH}")
    set(TARGET_NAME "analyze_iwyu_${program}")
    add_custom_target(
        ${TARGET_NAME}
        COMMAND
            ${CMAKE_CXX_INCLUDE_WHAT_YOU_USE} -p ${CMAKE_BINARY_DIR}
            -I${CMAKE_SOURCE_DIR}/include
            -I${CMAKE_SOURCE_DIR}/mocks
            ${CMAKE_SOURCE_DIR}/main.cpp
        COMMENT "Running IWYU Code Analysis"
        VERBATIM
    )
    # Add a dependency to make sure IWYU is run before building the main target
    add_dependencies(${program} ${TARGET_NAME})
endfunction()

function(enable_lizard_for_tests program SOURCE_DIRS)
    find_program(LIZARD lizard)

    if (NOT LIZARD)
        message(WARNING "Lizard not found. Code complexity analysis will not be performed.")
        return()
    endif ()

    # Create a list of source files using file globbing
    file(GLOB_RECURSE SOURCE_FILES
    ${SOURCE_DIRS}/*.cpp
    )
    # Add a custom target to run lizard
    set(TARGET_NAME "lizard_${program}")
    add_custom_target(
        ${TARGET_NAME}
        COMMAND ${CMAKE_COMMAND} -E env PATH="/home/ubuntu/.local/bin:$ENV{PATH}" lizard ${SOURCE_FILES}
        COMMENT "Running Lizard Code Complexity Analysis"
    )
    # Optionally, add a dependency to ensure Lizard runs before building your target
    add_dependencies(${program} ${TARGET_NAME})
endfunction()
