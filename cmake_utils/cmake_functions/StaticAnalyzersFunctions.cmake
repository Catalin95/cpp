# Cmake containing static analyzers functions for main app

function(enable_clang_tidy Program)
    find_program(CLANG_TIDY_BIN clang-tidy)

    if (NOT CLANG_TIDY_BIN)
        message(WARNING "clang-tidy not found. Static analysis checks will not be performed.")
        return()
    endif ()

    message(STATUS "clang-tidy will be used for main")
    set_target_properties(
        ${Program} PROPERTIES
        CXX_CLANG_TIDY "${CLANG_TIDY_BIN};--checks=-*,readability-*,readability-*,modernize-*,cppcoreguidelines-*,clang-*,performance-*,portability-*;--header-filter=^${CMAKE_CURRENT_SOURCE_DIR}/include/,^${CMAKE_CURRENT_SOURCE_DIR}/mocks/;--use-color"
    )
endfunction()

function(enable_cpp_check Program)
    find_program(CPPCHECK_BIN cppcheck)

    if (NOT CPPCHECK_BIN)
        message(WARNING "cppcheck not found. Static analysis checks will not be performed.")
        return()
    endif()

    message(STATUS "cppcheck will be used for main")
    set_target_properties(
        ${Program}  # Replace with your actual target name
        PROPERTIES
        CXX_CPPCHECK "${CPPCHECK_BIN};--enable=all;--std=c++${CMAKE_CXX_STANDARD};-I${CMAKE_SOURCE_DIR}/include/;-I${CMAKE_SOURCE_DIR}/mocks/;--suppress=missingIncludeSystem;--suppress=missingInclude;--quiet"
    )
endfunction()
