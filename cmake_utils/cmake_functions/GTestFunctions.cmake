# Cmake containing functions for googletest

function(fetch_google_test)
    include(FetchContent)
    FetchContent_Declare(
        googletest
        GIT_REPOSITORY https://github.com/google/googletest.git
        GIT_TAG v1.14.0
    )
    FetchContent_MakeAvailable(googletest)
endfunction()

function(set_googletest_include_dirs program)
    # Add include directories
    target_include_directories(${program} PUBLIC
        ${gtest_SOURCE_DIR}/include
        ${gmock_SOURCE_DIR}/include
    )
endfunction()

function(set_googletest_libraries program)
    # Link against Google Test
    target_link_libraries(${program} PUBLIC
        GTest::gtest
        GTest::gmock
        GTest::gtest_main
    )
endfunction()

function(set_googletest_framework program)
    if (NOT DEFINED TESTING_FRAMEWORK)
        set(TESTING_FRAMEWORK "GoogleTest")
    elseif (TESTING_FRAMEWORK NOT STREQUAL "GoogleTest")
        message(WARNING "Unsupported testing framework")
        return()
    endif ()

    if (TESTING_FRAMEWORK STREQUAL "GoogleTest")
        message(STATUS "Using testing framework: GoogleTest")
        find_package(GTest QUIET)

        if (NOT GTest_FOUND)
            message(STATUS "GoogleTest not found, fetching GoogleTest...")
            fetch_google_test()
            set_googletest_include_dirs(${program})
            set_googletest_libraries(${program})
            return()
        else()
            message(STATUS "GoogleTest found, using the installed version")
            set_googletest_libraries(${program})
            return()
        endif()
    endif ()
endfunction()
