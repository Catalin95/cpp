/**
 * @file interface.hpp
 * @brief Interface for different classes that might be used in the program.
 */

#pragma once

#include <cstdint>

namespace interfaces
{
class IExample
{
public:
    virtual ~IExample() = default;
    virtual auto print() -> void = 0;
};
}
