/**
 * @file work.hpp
 * @brief This file contains the declaration for the most implementation of the program.
 */

#pragma once

#include "interface.hpp"

#include <cstdint>
#include <memory>

namespace functions {}

namespace classes
{
    class Example
    {
    public:
        explicit Example(std::int32_t* input);
        ~Example() = default;

    private:
        std::unique_ptr<std::int32_t> m_num;
    };
};
