/**
 * @file mock.hpp
 * @brief This file contains mock classes for the program.
 */

#pragma once

#include "interface.hpp"
#include "work.hpp"
#include <gmock/gmock.h>

namespace mocks
{

// class MockExample : public interfaces::IExample
// {
// public:
//     ~MockExample() = default;
//     MOCK_METHOD0(print, void());
//
// };



// class MockExampleSecond : public classes::Example
// {
// public:
//     MockExampleSecond() : classes::Example(1, 2) {}
//     MOCK_METHOD0(print, void());
// };


}